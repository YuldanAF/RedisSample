# RedisSample
#### Setup
> 1. Clone https://gitlab.com/YuldanAF/RedisSample.git to project folder on your laptop
> 2. Restore Database Tested.bak to local or use server database as well
> 3. Open RedisSample.sln
> 4. Change App.config to your local connection db Tested 
> 5. Open redis-server.exe in redis-2.4.5-win32-win64
> 6. Build Console Project
> 7. Run Console

