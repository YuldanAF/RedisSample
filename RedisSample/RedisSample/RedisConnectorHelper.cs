﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisSample
{
    public class RedisConnectorHelper
    {
        static RedisConnectorHelper()
        {
            //string connectionString = "127.0.0.1:6379,ssl=false,password=dansaf1,abortConnect=false";
            string connectionString = "127.0.0.1:6379,ssl=false,abortConnect=false";


            RedisConnectorHelper.lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                return ConnectionMultiplexer.Connect(connectionString);
            });
        }

        private static Lazy<ConnectionMultiplexer> lazyConnection;

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}
