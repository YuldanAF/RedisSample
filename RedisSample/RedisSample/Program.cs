﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using System.Data.SqlClient;

namespace RedisSample
{
    class Program
    {

        static void Main(string[] args)
        {
            var program = new Program();

            Console.WriteLine("Input Database to use not redis");
            var choose = Console.ReadLine();
            if (choose == "Database")
            {
                DateTime awal = DateTime.Now;
                program.DBProgram();
                DateTime akhir = DateTime.Now;
                TimeSpan span = akhir.Subtract(awal);
                Console.WriteLine("Waktu Mulainya adalah  :" + awal);
                Console.WriteLine("Waktu Berakhirnya adalah  :" + akhir);
                //Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.TotalDays + " Hari " + span.TotalHours + " Jam " + span.TotalMinutes + " Menit " + span.TotalSeconds + " Detik");
                Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.ToString());
                Console.ReadLine();
            }
            else
            {
                DateTime awal = DateTime.Now;
                program.RedisProgram();
                DateTime akhir = DateTime.Now;
                TimeSpan span = akhir.Subtract(awal);
                Console.WriteLine("Waktu Mulainya adalah  :" + awal);
                Console.WriteLine("Waktu Berakhirnya adalah  :" + akhir);
                //Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.TotalDays + " Hari " + span.TotalHours + " Jam " + span.TotalMinutes + " Menit " + span.TotalSeconds + " Detik");
                Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.ToString());
                Console.ReadLine();
            }

            
        }
        
        public void DBProgram() {
            DateTime awalan = DateTime.Now;
            Console.WriteLine("Saving random data in cache");
            SaveDatabase();
            Console.WriteLine("Reading data from cache");
            ReadDatabase();
            DateTime akhiran = DateTime.Now;
            TimeSpan span = akhiran.Subtract(awalan);
            Console.WriteLine("Waktu Mulainya adalah  :" + awalan);
            Console.WriteLine("Waktu Berakhirnya adalah  :" + akhiran);
            //Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.TotalDays + " Hari " + span.TotalHours + " Jam " + span.TotalMinutes +" Menit " + span.TotalSeconds +" Detik");
            Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.ToString());
            Console.ReadLine();
        }
        public void ReadDatabase()
        {
            string connetionString = null;
            SqlConnection cnn;
            connetionString = "Data Source=localhost;Initial Catalog=Tested;User ID=sa;Password=sa123";
            cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
                Console.WriteLine("Connection Open ! ");
                String Name;
                String Device;
                SqlDataReader rdr = null;
                using (SqlCommand cmd = new SqlCommand("select * from Dump", cnn))
                    {
                    
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Device = rdr["Device"].ToString();
                        Name = rdr["Name"].ToString();
                 //       Console.WriteLine(Device + Name);
                    }
                
                }

                Console.WriteLine("Read Data From Database Success");
                Console.ReadLine();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not open connection ! ");
            }
        }
        public void SaveDatabase()
        {
            string connetionString = null;
            SqlConnection cnn;
            connetionString = "Data Source=localhost;Initial Catalog=Tested;User ID=sa;Password=sa123";
            cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
               Console.WriteLine("Connection Open ! ");
                var devicesCount = 10000;
                var rnd = new Random();
                for (int i = 1; i < devicesCount; i++)
                {
                    var value = rnd.Next(0, 10000);
                    using (SqlCommand cmd = new SqlCommand("Insert into Dump (Device, Name) values (@Device, @Name)", cnn))
                    {

                        cmd.Parameters.AddWithValue("@Device", $"Device_Status:{i}");
                        cmd.Parameters.AddWithValue("@Name", value);
                        cmd.ExecuteNonQuery();
                       // Console.WriteLine("Success,Added Data" + $"Device={i}");
                    }

                }
                Console.WriteLine("Add Data To Database Success");
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not open connection ! ");
            }
        }

        public void RedisProgram() {
          //  DateTime awal = DateTime.Now;
            Console.WriteLine("Saving random data in cache");
            SaveBigData();
            Console.WriteLine("Reading data from cache");
            ReadData();
            //DateTime akhir = DateTime.Now;
            //TimeSpan span = akhir.Subtract(awal);
            //Console.WriteLine("Waktu Mulainya adalah  :" + awal);
            //Console.WriteLine("Waktu Berakhirnya adalah  :" + akhir);
            //Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.TotalDays + " Hari " + span.TotalHours + " Jam " + span.TotalMinutes +" Menit " + span.TotalSeconds +" Detik");
            //Console.WriteLine("Lama waktu Eksekusinya adalah  : " + span.ToString());
            //Console.ReadLine();
               
        }

        public void ReadData()
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            var devicesCount = 10000;
            for (int i = 0; i < devicesCount; i++)
            {
                var value = cache.StringGet($"Device_Status:{i}");
                //Console.WriteLine($"Valor={value}");
            }
            Console.WriteLine("Read Data From Redis Success");
            //Console.WriteLine("Masukkan Keynya");
            //var key = Console.ReadLine();
            //var value = cache.StringGet(key);
            //Console.WriteLine("Isi dari keynya adalah   " + value);
        }

        public void SaveBigData()
        {
            var devicesCount = 10000;
            var rnd = new Random();
            var cache = RedisConnectorHelper.Connection.GetDatabase();

            for (int i = 1; i < devicesCount; i++)
            {
                var value = rnd.Next(0, 10000);
                cache.StringSet($"Device_Status:{i}", value);
            }
            //Console.WriteLine("Masukkan String Key");
            //var stringkey = Console.ReadLine();
            //Console.WriteLine("Masukkan Value");
            //var value = Console.ReadLine();
            //cache.StringSet(stringkey, value);
            Console.WriteLine("Save data to Redis Success");

        }
    }
    
}

